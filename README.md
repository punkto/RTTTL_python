# RTTTL
Nokia Ring Tone Text Transfer Language (RTTTL) parser for python with some scripts.

Forked from https://github.com/asdwsda/rtttl.git

## Installation

No instalation. For installation use https://github.com/asdwsda/rtttl.git instead.

## Usage of scripts
rtttl_file_to_c_format.py converts from a RTTTL file to a C format:
```
$ cat /tmp/rtttl/barbie.txt
Barbie girl:d=4,o=5,b=125:8g#,8e,8g#,8c#6,a,p,8f#,8d#,8f#,8b,g#,8f#,8e,p,8e,8c#,f#,c#,p,8f#,8e,g#,f#

$ python rtttl_file_to_c_format.py -i /tmp/rtttl/barbie.txt -o /tmp/barbie.h
Input rtttl file is /tmp/rtttl/barbie.txt
Output formated file is /tmp/barbie.h

$ cat /tmp/barbie.h #define SONG_LEN_BARBIE_GIRL 23
#define SONG_VEL_BARBIE_GIRL 125
    int notes_pitch_BARBIE_GIRL[SONG_LEN_BARBIE_GIRL] = {
        GX5, E5, GX5, CX6, A5, R, FX5, DX5, FX5, B5, GX5, FX5, E5, R, E5, CX5, FX5, CX5, R, FX5, E5, GX5, FX5};

    int notes_duration_BARBIE_GIRL[SONG_LEN_BARBIE_GIRL] = {
        4, 4, 4, 4, 8, 8, 4, 4, 4, 4, 8, 4, 4, 8, 4, 4, 8, 8, 8, 4, 4, 8, 8};

```
rtttl_dir_to_c_format.py converts the rtttl files in a directory in a C header file. use like this:

python rtttl_dir_to_c_format.py -i /tmp/rtttl_files/ -o /tmp/songs.h

## Usage as a lib
```python
>>> import pprint
>>> from rtttl import parse_rtttl

>>> barbie = parse_rtttl('Barbie girl:d=4,o=5,b=125:8g#,8e,8g#,8c#6,a,p,8f#,8d#,8f#,8b,g#,8f#,8e,p,8e,8c#,f#,c#,p,8f#,8e,g#,f#')

>>> pprint.pprint(barbie)
{'bpm': 125,
 'notes': [{'duration': 240.0,
            'duration_code': 4,
            'frequency': 830.6,
            'note_code': 'GX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 659.2,
            'note_code': 'E5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 830.6,
            'note_code': 'GX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 1108.8,
            'note_code': 'CX6'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 880.0,
            'note_code': 'A5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 0,
            'note_code': 'R'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 740.0,
            'note_code': 'FX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 622.2,
            'note_code': 'DX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 740.0,
            'note_code': 'FX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 987.8,
            'note_code': 'B5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 830.6,
            'note_code': 'GX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 740.0,
            'note_code': 'FX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 659.2,
            'note_code': 'E5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 0,
            'note_code': 'R'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 659.2,
            'note_code': 'E5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 554.4,
            'note_code': 'CX5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 740.0,
            'note_code': 'FX5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 554.4,
            'note_code': 'CX5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 0,
            'note_code': 'R'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 740.0,
            'note_code': 'FX5'},
           {'duration': 240.0,
            'duration_code': 4,
            'frequency': 659.2,
            'note_code': 'E5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 830.6,
            'note_code': 'GX5'},
           {'duration': 480.0,
            'duration_code': 8,
            'frequency': 740.0,
            'note_code': 'FX5'}],
 'title': 'Barbie girl'}
```
