#!/usr/bin/python

import sys, getopt
import pprint
from rtttl import parse_rtttl

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print 'test.py -i <inputfile> -o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <inputfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print 'Input rtttl file is', inputfile
    print 'Output formated file is', outputfile
    print
    work(inputfile, outputfile)

def work(inputfile, outputfile):
    rtttl_text = 'Barbie girl:d=4,o=5,b=125:8g#,8e,8g#,8c#6,a,p,8f#,8d#,8f#,8b,g#,8f#,8e,p,8e,8c#,f#,c#,p,8f#,8e,g#,f#'

    try:
        with open(inputfile) as file:
            rtttl_text = file.read()
    except:
        print "Problem using the input file, using the default RTTTL song."

    parsed_rtttl = parse_rtttl(rtttl_text)

    song_name_proper_format = parsed_rtttl["title"].upper().replace(" ", "_").replace("'", "_")

    song_length_define_name = "SONG_LEN_" + song_name_proper_format
    song_velocity_define_name = "SONG_VEL_" + song_name_proper_format
    notes_pitch_array_name = "notes_pitch_" + song_name_proper_format
    notes_duration_array_name = "notes_duration_" + song_name_proper_format

    note_code_list = [note["note_code"] for note in parsed_rtttl["notes"]]
    note_duration_list = [str(note["duration_code"]) for note in parsed_rtttl["notes"]]
    
    output = "#define " + song_length_define_name + " " +  str((len(parsed_rtttl["notes"]))) + '\n' \
        + "#define " + song_velocity_define_name + " " +  str(parsed_rtttl["bpm"]) + '\n' \
        + "    int " + notes_pitch_array_name + "[" + song_length_define_name + "] = {\n" \
        + "        " + ", ".join(note_code_list) + "};\n\n" \
        + "    int " + notes_duration_array_name + "[" + song_length_define_name + "] = {\n" \
        + "        " + ", ".join(note_duration_list) + "};\n"

    try:
        with open(outputfile, "w") as file:
            file.write(output)
    except:
        print output


if __name__ == "__main__":
    main(sys.argv[1:])

