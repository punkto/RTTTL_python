#!/usr/bin/python

import sys, getopt, glob
import pprint
import rtttl

def main(argv):
    inputdir = '/tmp/rtttl_songs/'
    outputfile = '/tmp/jukeboxsongs.h'
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["idir=","ofile="])
    except getopt.GetoptError:
        print 'test.py -i <inputdir> -o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'rtttl_dir_to_c_format.py -i <inputdir> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--idir"):
            inputdir = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    print 'Input rtttl files directory is', inputdir
    print 'Output formated file is', outputfile
    print
    work(inputdir, outputfile)


def work(inputdir, outputfile):
    all_songs_data = []
    for inputfile in glob.glob(inputdir+"/*.txt"):
        try:
            all_songs_data.append(file_to_data(inputfile))
        except rtttl.exceptions.InvalidNoteError:
            print inputfile, "File could not be parsed"
    write_to_file(outputfile, all_songs_data)

def file_to_data(inputfile):
    rtttl_text = 'Barbie girl:d=4,o=5,b=125:8g#,8e,8g#,8c#6,a,p,8f#,8d#,8f#,8b,g#,8f#,8e,p,8e,8c#,f#,c#,p,8f#,8e,g#,f#'

    try:
        with open(inputfile) as file:
            rtttl_text = file.read()
    except:
        print "Problem using the input file, using the default RTTTL song."

    parsed_rtttl = rtttl.parse_rtttl(rtttl_text)

    song_name_proper_format = parsed_rtttl["title"].upper().replace(" ", "_").replace("'", "_")

    song_length_define_name = "SONG_LEN_" + song_name_proper_format
    song_velocity_define_name = "SONG_VEL_" + song_name_proper_format
    notes_pitch_array_name = "notes_pitch_" + song_name_proper_format
    notes_duration_array_name = "notes_duration_" + song_name_proper_format

    note_code_list = [note["note_code"] for note in parsed_rtttl["notes"]]
    note_duration_list = [str(note["duration_code"]) for note in parsed_rtttl["notes"]]
    
    output = "#define " + song_length_define_name + " " +  str((len(parsed_rtttl["notes"]))) + '\n' \
        + "#define " + song_velocity_define_name + " " +  str(parsed_rtttl["bpm"]) + '\n' \
        + "    int " + notes_pitch_array_name + "[" + song_length_define_name + "] = {\n" \
        + "        " + ", ".join(note_code_list) + "};\n" \
        + "    int " + notes_duration_array_name + "[" + song_length_define_name + "] = {\n" \
        + "        " + ", ".join(note_duration_list) + "};\n"

    order_name = "SONG_" + song_name_proper_format + "_ORDER"

    return {'defines':output, 'order_name': order_name, 'length_name':song_length_define_name, 'velocity_name': song_velocity_define_name, 'notes_pitch_array_name': notes_pitch_array_name, 'notes_duration_array_name':notes_duration_array_name}



def write_to_file(outputfile, data):

    with open(outputfile, "w") as file:
        file.write('''
#include "libmusic.h"

struct Song {
	int length;
	int bpm;
	int *notes_pitch;
	int *notes_duration;
};

''')
        file.write("#define JUKEBOX_SONGS_TOTAL_SONGS " + str(len(data)) \
         + '\n'+ "struct Song jukebox_song_list[JUKEBOX_SONGS_TOTAL_SONGS];\n\n")

        index = 0
        for i in data:
            file.write("#define " + i['order_name'] + " " + str(index) + '\n')
            index = index + 1
            file.write(i['defines'] + '\n')
        
        file.write('''
// Returns the number of songs in the jukebox
int init_jukebox_songs() {

'''
)
        for i in data:
            file.write("    jukebox_song_list[" + i['order_name'] + "].length = " + i['length_name'] + ';\n')
            file.write("    jukebox_song_list[" + i['order_name'] + "].bpm = " + i['velocity_name'] + ';\n')
            file.write("    jukebox_song_list[" + i['order_name'] + "].notes_pitch = " + i['notes_pitch_array_name'] + ';\n')
            file.write("    jukebox_song_list[" + i['order_name'] + "].notes_duration = " + i['notes_duration_array_name'] + ';\n\n')

        file.write('''
    return JUKEBOX_SONGS_TOTAL_SONGS;
}

int get_total_songs_number() {
	return JUKEBOX_SONGS_TOTAL_SONGS;
}
'''
)

if __name__ == "__main__":
    main(sys.argv[1:])

